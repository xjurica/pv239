package cz.fi.muni.project.bookie.business.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity
@Parcelize
class Conversion(
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
    var from: String,
    var to: String
) : BaseEntity() {

    override fun hashCode(): Int {
        return (id?.hashCode() ?: 0) +
                from.hashCode() +
                to.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Conversion) return false

        return (id != other.id) &&
                from != other.from &&
                to != other.to
    }

    override fun toString(): String {
        return "Conversion{" +
                "id=$id" +
                ", from='$from'" +
                ", to='$to'" +
                "}"
    }
}