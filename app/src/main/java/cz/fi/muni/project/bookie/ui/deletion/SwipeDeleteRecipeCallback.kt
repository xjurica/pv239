package cz.fi.muni.project.bookie.ui.deletion

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import cz.fi.muni.project.bookie.business.dao.CategoryDao
import cz.fi.muni.project.bookie.business.dao.RecipeDao
import cz.fi.muni.project.bookie.business.model.Recipe
import cz.fi.muni.project.bookie.ui.adapters.RecipeAdapter
import kotlinx.coroutines.runBlocking

class SwipeDeleteRecipeCallback(private val adapter: RecipeAdapter,
                                private val childFragmentManager: FragmentManager,
                                context: Context?,
                                private val recipeDao: RecipeDao,
                                private val categoryDao: CategoryDao,
                                private val uiCallback: (() -> Unit)? = null)
    : SwipeDeleteCallback(context) {

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position = viewHolder.adapterPosition
        val recipes = adapter.getData() ?: return
        val recipe = recipes[position]

        val dialog = ConfirmDeletionDialogFragment(recipe.title)
        dialog.listener = object : ConfirmDeletionDialogListener {
            override fun onConfirmDeletionResult(confirmed: Boolean) {
                if (confirmed) {
                    var recipesWithCategory: List<Recipe>? = null
                    if (recipe.categoryId != null) {
                        recipesWithCategory = runBlocking {
                            recipeDao.findByCategoryId(recipe.categoryId!!)
                        }
                    }

                    runBlocking {
                        // If the recipe's category is not used anymore, delete it too
                        if (recipesWithCategory != null && recipesWithCategory.size == 1) {
                            categoryDao.deleteById(recipe.categoryId!!)
                        }
                        recipeDao.delete(recipe)
                    }
                    adapter.deleteItem(position)
                    uiCallback?.invoke()
                } else {
                    adapter.notifyItemChanged(position)
                }
            }
        }
        dialog.show(childFragmentManager, "ConfirmDeletionDialogFragment")
    }
}