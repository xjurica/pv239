package cz.fi.muni.project.bookie

import android.app.Application
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import cz.fi.muni.project.bookie.business.db.CookbookDatabase


class Cookbook : Application() {
    lateinit var db: CookbookDatabase

    override fun onCreate() {
        super.onCreate()

        val rdc: RoomDatabase.Callback = object : RoomDatabase.Callback() {
            // Called when the db is created for the first time
            override fun onCreate(db: SupportSQLiteDatabase) {
                db.execSQL("INSERT INTO Conversion ('from', 'to') VALUES ('1 teaspoon', '5 milliliters')")
                db.execSQL("INSERT INTO Conversion ('from', 'to') VALUES ('1 tablespoon', '15 milliliters')")
                db.execSQL("INSERT INTO Conversion ('from', 'to') VALUES ('1 cup', '240 milliliters')")
                db.execSQL("INSERT INTO Conversion ('from', 'to') VALUES ('1 pint', '475 milliliters')")
                db.execSQL("INSERT INTO Conversion ('from', 'to') VALUES ('1 gallon', '3.8 liters')")
                db.execSQL("INSERT INTO Conversion ('from', 'to') VALUES ('A pinch', '0.3 grams')")
                db.execSQL("INSERT INTO Conversion ('from', 'to') VALUES ('A handful', 'Volume of (preferably your) hand')")
            }
        }

        db = Room.databaseBuilder(
            applicationContext,
            CookbookDatabase::class.java, "cookbook"
        ).addCallback(rdc)
            .build()
    }
}