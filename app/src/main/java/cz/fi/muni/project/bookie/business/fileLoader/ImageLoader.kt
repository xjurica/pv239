package cz.fi.muni.project.bookie.business.fileLoader

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.ImageDecoder
import android.graphics.ImageDecoder.Source
import android.graphics.Matrix
import android.net.Uri
import android.widget.ImageView
import androidx.activity.result.ActivityResultCaller
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts.PickVisualMedia
import androidx.fragment.app.FragmentActivity
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min


class ImageLoader(
    // When called from Fragment, put Fragment. When called from Activity, put Activity.
    activityResultCaller: ActivityResultCaller,
    // Put always Activity.
    private val activity: FragmentActivity
) {
    private var imageView: ImageView? = null
    private var finalImage: Bitmap? = null
    private val pickMedia = activityResultCaller.registerForActivityResult(PickVisualMedia()) {
        val imageUri = it
        if(imageUri != null && imageView != null) {
            finalImage = editImage(convertUri(imageUri))
            imageView!!.setImageBitmap(finalImage)
        }
    }

    /**
     * Currently this function always returns null, and the actual result,
     * is a Bitmap set in imageView.
     *
     * The reason is we do not know how to stop the function after pickMedia.launch, so that it can
     * wait until users input. Right now it just continues and returns null
     */
    fun pick(imageView: ImageView): Bitmap? {
        this.imageView = imageView
        pickMedia.launch(PickVisualMediaRequest(PickVisualMedia.ImageOnly))

        return finalImage
    }

    private fun convertUri(uri: Uri): Bitmap {
        val source: Source = ImageDecoder.createSource(activity.contentResolver, uri)
        return ImageDecoder.decodeBitmap(source)
    }

    /**
     * Edits the given bitmap by resizing it, blurring the background, and overlaying the resized image on the blurred background.
     *
     * @param srcBmp The source bitmap to be edited.
     * @return The edited bitmap with the resized image overlaid on the blurred background.
     */
    private fun editImage(srcBmp: Bitmap) : Bitmap {
        val maxSize = max(srcBmp.height, srcBmp.width).toFloat()
        val targetSize = 1000 // Picked after considering quality/efficiency as well as DB crashes
        val resizeFactor = targetSize / maxSize

        // The images are resized to targetSize because they can be too big and then DB crashes
        val resizedBmp = Bitmap.createScaledBitmap(
            srcBmp.copy(Bitmap.Config.ARGB_8888, true),
            (srcBmp.width * resizeFactor).toInt(),
            (srcBmp.height * resizeFactor).toInt(),
            true
        )

        // Ignore square images since there will be no effect
        if (srcBmp.height != srcBmp.width) {
            val backgroundBmp = Bitmap.createScaledBitmap(srcBmp, targetSize, targetSize, true)
            val blurredBmp = fastblur(backgroundBmp, 1F, 80)

            return overlay(blurredBmp, resizedBmp)
        }
        return resizedBmp
    }

    /**
     * Overlays one bitmap (bmp2) onto another bitmap (bmp1) at the center.
     *
     * @param bmp1 The larger or equal-sized bitmap onto which the other bitmap will be overlaid.
     * @param bmp2 The smaller bitmap to be overlaid onto bmp1.
     * @return A new bitmap with bmp2 overlaid onto bmp1 at the center.
     * @throws IllegalArgumentException If bmp1 is smaller than bmp2 in either width or height.
     */
    private fun overlay(bmp1: Bitmap, bmp2: Bitmap): Bitmap {
        val x = (bmp1.width - bmp2.width) / 2
        val y = (bmp1.height - bmp2.height) / 2

        val bmOverlay = Bitmap.createBitmap(bmp1.width, bmp1.height, bmp1.config)
        val canvas = Canvas(bmOverlay)
        canvas.drawBitmap(bmp1, Matrix(), null)
        canvas.drawBitmap(bmp2, x.toFloat(), y.toFloat(), null)

        return bmOverlay
    }

    /**
     * Applies a fast blur effect to the given bitmap.
     *
     * @param originalBitmap The original bitmap to be blurred.
     * @param scale The scale factor for resizing the bitmap before applying the blur effect.
     * @param radius The radius of the blur effect. Must be greater than or equal to 1.
     * @return A blurred version of the original bitmap.
     */
    private fun fastblur(originalBitmap: Bitmap, scale: Float, radius: Int): Bitmap {
        var sentBitmap = originalBitmap.copy(Bitmap.Config.ARGB_8888, true)
        val width = Math.round(sentBitmap.width * scale)
        val height = Math.round(sentBitmap.height * scale)
        sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false)
        val bitmap = sentBitmap.copy(sentBitmap.config, true)
        val w = bitmap.width
        val h = bitmap.height
        val pix = IntArray(w * h)
        bitmap.getPixels(pix, 0, w, 0, 0, w, h)
        val wm = w - 1
        val hm = h - 1
        val wh = w * h
        val div = radius + radius + 1
        val r = IntArray(wh)
        val g = IntArray(wh)
        val b = IntArray(wh)
        var rsum: Int
        var gsum: Int
        var bsum: Int
        var x: Int
        var y: Int
        var i: Int
        var p: Int
        var yp: Int
        var yi: Int
        var yw: Int
        val vmin = IntArray(max(w.toDouble(), h.toDouble()).toInt())
        var divsum = div + 1 shr 1
        divsum *= divsum
        val dv = IntArray(256 * divsum)
        i = 0
        while (i < 256 * divsum) {
            dv[i] = i / divsum
            i++
        }
        yi = 0
        yw = yi
        val stack = Array(div) {
            IntArray(
                3
            )
        }
        var stackpointer: Int
        var stackstart: Int
        var sir: IntArray
        var rbs: Int
        val r1 = radius + 1
        var routsum: Int
        var goutsum: Int
        var boutsum: Int
        var rinsum: Int
        var ginsum: Int
        var binsum: Int
        y = 0
        while (y < h) {
            bsum = 0
            gsum = bsum
            rsum = gsum
            boutsum = rsum
            goutsum = boutsum
            routsum = goutsum
            binsum = routsum
            ginsum = binsum
            rinsum = ginsum
            i = -radius
            while (i <= radius) {
                p = pix[(yi + min(wm.toDouble(), max(i.toDouble(), 0.0))).toInt()]
                sir = stack[i + radius]
                sir[0] = p and 0xff0000 shr 16
                sir[1] = p and 0x00ff00 shr 8
                sir[2] = p and 0x0000ff
                rbs = (r1 - abs(i.toDouble())).toInt()
                rsum += sir[0] * rbs
                gsum += sir[1] * rbs
                bsum += sir[2] * rbs
                if (i > 0) {
                    rinsum += sir[0]
                    ginsum += sir[1]
                    binsum += sir[2]
                } else {
                    routsum += sir[0]
                    goutsum += sir[1]
                    boutsum += sir[2]
                }
                i++
            }
            stackpointer = radius
            x = 0
            while (x < w) {
                r[yi] = dv[rsum]
                g[yi] = dv[gsum]
                b[yi] = dv[bsum]
                rsum -= routsum
                gsum -= goutsum
                bsum -= boutsum
                stackstart = stackpointer - radius + div
                sir = stack[stackstart % div]
                routsum -= sir[0]
                goutsum -= sir[1]
                boutsum -= sir[2]
                if (y == 0) {
                    vmin[x] = min((x + radius + 1).toDouble(), wm.toDouble()).toInt()
                }
                p = pix[yw + vmin[x]]
                sir[0] = p and 0xff0000 shr 16
                sir[1] = p and 0x00ff00 shr 8
                sir[2] = p and 0x0000ff
                rinsum += sir[0]
                ginsum += sir[1]
                binsum += sir[2]
                rsum += rinsum
                gsum += ginsum
                bsum += binsum
                stackpointer = (stackpointer + 1) % div
                sir = stack[stackpointer % div]
                routsum += sir[0]
                goutsum += sir[1]
                boutsum += sir[2]
                rinsum -= sir[0]
                ginsum -= sir[1]
                binsum -= sir[2]
                yi++
                x++
            }
            yw += w
            y++
        }
        x = 0
        while (x < w) {
            bsum = 0
            gsum = bsum
            rsum = gsum
            boutsum = rsum
            goutsum = boutsum
            routsum = goutsum
            binsum = routsum
            ginsum = binsum
            rinsum = ginsum
            yp = -radius * w
            i = -radius
            while (i <= radius) {
                yi = (max(0.0, yp.toDouble()) + x).toInt()
                sir = stack[i + radius]
                sir[0] = r[yi]
                sir[1] = g[yi]
                sir[2] = b[yi]
                rbs = (r1 - abs(i.toDouble())).toInt()
                rsum += r[yi] * rbs
                gsum += g[yi] * rbs
                bsum += b[yi] * rbs
                if (i > 0) {
                    rinsum += sir[0]
                    ginsum += sir[1]
                    binsum += sir[2]
                } else {
                    routsum += sir[0]
                    goutsum += sir[1]
                    boutsum += sir[2]
                }
                if (i < hm) {
                    yp += w
                }
                i++
            }
            yi = x
            stackpointer = radius
            y = 0
            while (y < h) {

                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] =
                    -0x1000000 and pix[yi] or (dv[rsum] shl 16) or (dv[gsum] shl 8) or dv[bsum]
                rsum -= routsum
                gsum -= goutsum
                bsum -= boutsum
                stackstart = stackpointer - radius + div
                sir = stack[stackstart % div]
                routsum -= sir[0]
                goutsum -= sir[1]
                boutsum -= sir[2]
                if (x == 0) {
                    vmin[y] = (min((y + r1).toDouble(), hm.toDouble()) * w).toInt()
                }
                p = x + vmin[y]
                sir[0] = r[p]
                sir[1] = g[p]
                sir[2] = b[p]
                rinsum += sir[0]
                ginsum += sir[1]
                binsum += sir[2]
                rsum += rinsum
                gsum += ginsum
                bsum += binsum
                stackpointer = (stackpointer + 1) % div
                sir = stack[stackpointer]
                routsum += sir[0]
                goutsum += sir[1]
                boutsum += sir[2]
                rinsum -= sir[0]
                ginsum -= sir[1]
                binsum -= sir[2]
                yi += w
                y++
            }
            x++
        }
        bitmap.setPixels(pix, 0, w, 0, 0, w, h)
        return bitmap
    }
}