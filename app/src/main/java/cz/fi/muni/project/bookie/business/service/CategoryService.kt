package cz.fi.muni.project.bookie.business.service

import cz.fi.muni.project.bookie.business.dao.CategoryDao
import cz.fi.muni.project.bookie.business.model.Category
import kotlinx.coroutines.runBlocking

class CategoryService(
    private val categoryDao: CategoryDao
) {
    fun create(name: String): Long {
        val category = Category(name = name)

        return runBlocking {
            categoryDao.create(category)[0]
        }
    }

    fun getId(name: String, createWhenMissing: Boolean = false): Long? {
        val categories = runBlocking {
            categoryDao.findByName(name)
        }

        return if (categories.isNotEmpty()) categories[0].id!! else if (createWhenMissing)
            create(name) else null
    }

    fun getName(id: Long): String? {
        val categories = runBlocking {
            categoryDao.findById(id)
        }

        return if (categories.isNotEmpty()) categories[0].name else null
    }

    fun getAllNames(): Array<String> {
        val names: MutableList<String> = ArrayList()

        for(category in runBlocking {
            categoryDao.findAll()
        }) {
            names += category.name
        }

        return names.toTypedArray()
    }
}