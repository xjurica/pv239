package cz.fi.muni.project.bookie.ui.deletion

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import cz.fi.muni.project.bookie.R

interface ConfirmDeletionDialogListener {
    fun onConfirmDeletionResult(confirmed: Boolean)
}

class ConfirmDeletionDialogFragment(private val itemToDelete: String) : DialogFragment() {
    lateinit var listener: ConfirmDeletionDialogListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val activity = requireActivity()
        val dialogView = View.inflate(activity, R.layout.dialog_confirm_deletion, null)
        val alertDialog = AlertDialog.Builder(activity, R.style.CustomDialogTheme).create()

        dialogView.findViewById<TextView>(R.id.text_view_item_to_delete).text = itemToDelete

        dialogView.findViewById<Button>(R.id.button_confirm_deletion_no).setOnClickListener {
            listener.onConfirmDeletionResult(false)
            alertDialog.dismiss()
        }

        dialogView.findViewById<Button>(R.id.button_confirm_deletion_yes).setOnClickListener {
            listener.onConfirmDeletionResult(true)
            alertDialog.dismiss()
        }

        alertDialog.setView(dialogView)
        return alertDialog
    }
}
