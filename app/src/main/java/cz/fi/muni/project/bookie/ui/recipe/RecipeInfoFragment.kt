package cz.fi.muni.project.bookie.ui.recipe

import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import com.wajahatkarim3.easyvalidation.core.view_ktx.validator
import cz.fi.muni.project.bookie.R
import cz.fi.muni.project.bookie.RecipesActivity
import cz.fi.muni.project.bookie.business.fileLoader.ImageLoader
import cz.fi.muni.project.bookie.business.imageConverter.BitmapToByteArrayConverter
import cz.fi.muni.project.bookie.business.imageConverter.ByteArrayToBitmapConverter
import cz.fi.muni.project.bookie.business.model.Recipe
import cz.fi.muni.project.bookie.databinding.FragmentRecipeInfoBinding
import kotlinx.coroutines.runBlocking


class RecipeInfoFragment : Fragment() {
    private lateinit var binding: FragmentRecipeInfoBinding
    private lateinit var imageLoader: ImageLoader
    private lateinit var editTextTitle: EditText
    private lateinit var autocompleteTextViewCategory: EditText
    private lateinit var editTextDuration: EditText
    private lateinit var editTextPortions: EditText
    private lateinit var editTextIngredients: EditText
    private lateinit var editTextInstructions: EditText


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRecipeInfoBinding.inflate(inflater, container, false)
        imageLoader = ImageLoader(this, requireActivity())
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editTextTitle = binding.editTextRecipeTitle
        autocompleteTextViewCategory = binding.autocompleteTextViewRecipeCategory
        ArrayAdapter(
            requireContext(),
            R.layout.autocomplete_item_category,
            R.id.text_view_autocomplete,
            (activity as RecipesActivity).categoryService.getAllNames()
        ).also {
            (autocompleteTextViewCategory as AutoCompleteTextView).setAdapter(it)
        }
        editTextDuration = binding.editTextRecipeDuration
        editTextPortions = binding.editTextRecipePortions
        editTextIngredients = binding.editTextRecipeIngredients
        editTextInstructions = binding.editTextRecipeInstructions

        setEditTextColor(binding.textViewRecipeIngredientsLabel)

        val args = RecipeInfoFragmentArgs.fromBundle(requireArguments())
        val recipe = args.recipe

        if (recipe.image != null) {
            binding.imageViewRecipe.setImageBitmap(
                ByteArrayToBitmapConverter(recipe.image!!).convert()
            )
        }

        val floatingButton = binding.floatingActionButtonEditInfo
        val saveButton = binding.buttonSaveChanges
        val imageButton = binding.buttonChangeImage


        floatingButton.setOnClickListener {
            floatingButton.visibility = View.GONE
            saveButton.visibility = View.VISIBLE
            imageButton.visibility = View.VISIBLE

            editTextDuration.setText(recipe.duration.toString())
            editTextDuration.postInvalidate()
            editTextPortions.setText(recipe.portions.toString())
            editTextPortions.postInvalidate()
            enableEditTexts(true)
        }

        binding.buttonSaveChanges.setOnClickListener {
            if (validateInput()) {
                saveButton.visibility = View.GONE
                imageButton.visibility = View.GONE
                floatingButton.visibility = View.VISIBLE
                enableEditTexts(false)

                updateRecipe(recipe)
                setInformation(recipe)
            }
        }

        val imageView = binding.imageViewRecipe
        imageButton.setOnClickListener {
            imageLoader.pick(imageView)
        }

        setInformation(recipe)

        enableEditTexts(false)
    }

    private fun enableEditTexts(boolean: Boolean) {
        editTextTitle.isEnabled = boolean
        autocompleteTextViewCategory.isEnabled = boolean
        editTextDuration.isEnabled = boolean
        editTextPortions.isEnabled = boolean
        editTextIngredients.isEnabled = boolean
        editTextInstructions.isEnabled = boolean

        if (boolean) {
            val drawable =
                AppCompatResources.getDrawable(requireContext(), R.drawable.edit_text_shape)
            editTextTitle.background = drawable
            autocompleteTextViewCategory.background = drawable
            editTextDuration.background = drawable
            editTextPortions.background = drawable
            editTextIngredients.background = drawable
            editTextInstructions.background = drawable
        } else {
            editTextTitle.background = null
            autocompleteTextViewCategory.background = null
            editTextDuration.background = null
            editTextPortions.background = null
            editTextIngredients.background = null
            editTextInstructions.background = null
        }

        editTextTitle.invalidate()
        autocompleteTextViewCategory.invalidate()
        editTextDuration.invalidate()
        editTextPortions.invalidate()
        editTextIngredients.invalidate()
        editTextInstructions.invalidate()
    }

    private fun setInformation(recipe: Recipe) {
        val durationContent = recipe.duration.toString() + " minutes"
        val portionsContent = recipe.portions.toString() + " portions"

        editTextTitle.setText(recipe.title)
        if (recipe.categoryId != null) {
            autocompleteTextViewCategory.setText(
                (activity as RecipesActivity).categoryService.getName(recipe.categoryId!!)
            )
        }
        editTextDuration.setText(durationContent)
        editTextPortions.setText(portionsContent)
        editTextIngredients.setText(recipe.ingredients)
        editTextInstructions.setText(recipe.instructions)
    }

    private fun validateInput(): Boolean {

        editTextTitle.error = null
        val titleCheck = editTextTitle.validator()
            .nonEmpty("Title must be filled!")
            .addErrorCallback {
                editTextTitle.error = it
            }
            .check()

        editTextDuration.error = null
        var durationCheck = editTextDuration.validator()
            .nonEmpty("Duration must be filled!")
            .validNumber()
            .greaterThanOrEqual(0)
            .addErrorCallback {
                editTextDuration.error = it
            }
            .check()
        if (durationCheck && editTextDuration.text.toString().toIntOrNull() == null) {
            durationCheck = false
            editTextDuration.error = "Number must be below 2147483648"
        }

        editTextPortions.error = null
        var portionsCheck = editTextPortions.validator()
            .nonEmpty("Portions must be filled!")
            .validNumber()
            .greaterThanOrEqual(0)
            .addErrorCallback {
                editTextPortions.error = it
            }
            .check()
        if (portionsCheck && editTextPortions.text.toString().toIntOrNull() == null) {
            portionsCheck = false
            editTextPortions.error = "Number must be below 2147483648"
        }

        return titleCheck &&
                durationCheck &&
                portionsCheck
    }

    private fun updateRecipe(recipe: Recipe) {
        val imageView = binding.imageViewRecipe
        val bd: BitmapDrawable = imageView.drawable as BitmapDrawable
        val bitmap = bd.bitmap
        val blob = BitmapToByteArrayConverter(bitmap).convert()
        recipe.image = blob

        val categoryString = autocompleteTextViewCategory.text.toString().ifBlank {"Unspecified"}
        val categoryId = updateCategory(categoryString, recipe.categoryId)

        recipe.title = editTextTitle.text.toString()
        recipe.categoryId = categoryId
        recipe.duration = editTextDuration.text.toString().toIntOrNull()
        recipe.portions = editTextPortions.text.toString().toIntOrNull()
        recipe.ingredients = editTextIngredients.text.toString()
        recipe.instructions = editTextInstructions.text.toString()

        runBlocking {
            (activity as RecipesActivity).recipeDao.update(recipe)
        }
    }

    private fun updateCategory(name: String, originalId: Long?): Long {
        val currentId = (activity as RecipesActivity)
            .categoryService
            .getId(name, createWhenMissing = true)

        // Try to free old category if it is not used anymore
        if (originalId != null && currentId != originalId) {
            val recipesWithCategory = runBlocking {
                (activity as RecipesActivity).recipeDao.findByCategoryId(originalId)
            }

            if (recipesWithCategory.size == 1) {
                runBlocking {
                    (activity as RecipesActivity).categoryDao.deleteById(originalId)
                }
            }
        }

        return currentId!!
    }

    private fun setEditTextColor(textView: TextView) {
        val color: Int = textView.currentTextColor
        editTextTitle.setTextColor(color)
        autocompleteTextViewCategory.setTextColor(color)
        editTextDuration.setTextColor(color)
        editTextPortions.setTextColor(color)
        editTextIngredients.setTextColor(color)
        editTextInstructions.setTextColor(color)
    }
}