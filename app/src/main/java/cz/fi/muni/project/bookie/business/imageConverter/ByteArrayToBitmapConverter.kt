package cz.fi.muni.project.bookie.business.imageConverter

import android.graphics.Bitmap
import android.graphics.BitmapFactory

class ByteArrayToBitmapConverter (
    private val data: ByteArray
) {
    fun convert(): Bitmap {
        return BitmapFactory.decodeByteArray(data, 0, data.size)
    }
}