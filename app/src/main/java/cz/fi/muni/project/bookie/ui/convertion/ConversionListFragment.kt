package cz.fi.muni.project.bookie.ui.convertion

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import cz.fi.muni.project.bookie.ConversionsActivity
import cz.fi.muni.project.bookie.databinding.FragmentConversionListBinding
import cz.fi.muni.project.bookie.ui.adapters.ConversionAdapter
import cz.fi.muni.project.bookie.ui.deletion.SwipeDeleteConversionCallback
import kotlinx.coroutines.runBlocking

class ConversionListFragment : Fragment() {
    private lateinit var binding: FragmentConversionListBinding

    private val adapter = ConversionAdapter(
        onClick = { comparison ->
            Toast.makeText(context, comparison.toString(), Toast.LENGTH_SHORT)
        }
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentConversionListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter

        val addButton = binding.floatingActionButtonAddConversion
        addButton.setOnClickListener {
            ConversionAddDialogFragment(this).show(childFragmentManager, "Add portion comparison")
        }

        updateAdapter()

        enableSwipeToDelete(adapter)
    }

    private fun enableSwipeToDelete(adapter: ConversionAdapter) {
        val swipeToDeleteCallback = SwipeDeleteConversionCallback(
            adapter,
            childFragmentManager,
            context,
            (activity as ConversionsActivity).conversionDao
        )
        val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchHelper.attachToRecyclerView(binding.recyclerView)
    }

    fun updateAdapter() {
        val conversions = runBlocking {
            (activity as ConversionsActivity).conversionDao.findAll()
        }
        if (conversions.isNotEmpty()) {
            binding.recyclerView.visibility = View.VISIBLE
            binding.textView.visibility = View.GONE

            adapter.submitList(conversions)
        }
    }
}