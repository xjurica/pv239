package cz.fi.muni.project.bookie

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import cz.fi.muni.project.bookie.business.dao.CategoryDao
import cz.fi.muni.project.bookie.business.dao.RecipeDao
import cz.fi.muni.project.bookie.business.service.CategoryService
import cz.fi.muni.project.bookie.databinding.ActivityRecipesBinding

class RecipesActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRecipesBinding
    lateinit var recipeDao: RecipeDao
    lateinit var categoryDao: CategoryDao
    lateinit var categoryService: CategoryService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        recipeDao = (application as Cookbook).db.recipeDao()
        categoryDao = (application as Cookbook).db.categoryDao()
        categoryService = CategoryService(categoryDao)
        binding = ActivityRecipesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.hide()

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_recipes)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_recipes, R.id.navigation_conversions
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.navigation_recipe_info ||
                destination.id == R.id.navigation_add_recipe
            ) {
                navView.visibility = View.GONE
            } else {
                navView.visibility = View.VISIBLE
            }
        }
    }
}