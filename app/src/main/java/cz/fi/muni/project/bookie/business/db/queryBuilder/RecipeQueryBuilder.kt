package cz.fi.muni.project.bookie.business.db.queryBuilder

import androidx.sqlite.db.SimpleSQLiteQuery

class RecipeQueryBuilder {
    private var title: String? = null
    private var category: String? = null
    private var durationFrom: Int? = null
    private var durationTo: Int? = null
    private var portionsFrom: Int? = null
    private var portionsTo: Int? = null
    private var ingredients: String? = null
    private var instructions: String? = null

    fun filterByTitle(title: String): RecipeQueryBuilder {
        this.title = title
        return this
    }

    fun filterByCategory(category: String): RecipeQueryBuilder {
        this.category = category
        return this
    }

    fun filterByDuration(durationFrom: Int, durationTo: Int): RecipeQueryBuilder {
        this.durationFrom = durationFrom
        this.durationTo = durationTo
        return this
    }

    fun filterByPortions(portionsFrom: Int, portionsTo: Int): RecipeQueryBuilder {
        this.portionsFrom = portionsFrom
        this.portionsTo = portionsTo
        return this
    }

    fun filterByIngredients(ingredients: String): RecipeQueryBuilder {
        this.ingredients = ingredients
        return this
    }

    fun filterByInstructions(instructions: String): RecipeQueryBuilder {
        this.instructions = instructions
        return this
    }

    fun build(): SimpleSQLiteQuery {
        val queryBase = "SELECT * FROM recipe AS r"
        var queryJoins = ""
        var queryConditions = ""

        val params: MutableList<Any> = ArrayList()

        if (isFiltered()) {
            queryConditions += " WHERE"

            if (title != null) {
                queryConditions += " r.title LIKE ? AND"
                params.add("%$title%")
            }
            if (category != null) {
                queryJoins += " JOIN category AS c ON r.categoryId = c.id"
                queryConditions += " c.name LIKE ? AND"
                params.add("%$category%")
            }
            if (durationFrom != null && durationTo != null) {
                queryConditions += " r.duration BETWEEN ? AND ? AND"
                params.add(durationFrom!!)
                params.add(durationTo!!)
            }
            if (portionsFrom != null && portionsTo != null) {
                queryConditions += " r.portions BETWEEN ? AND ? AND"
                params.add(portionsFrom!!)
                params.add(portionsTo!!)
            }
            if (ingredients != null) {
                queryConditions += " r.ingredients LIKE ? AND"
                params.add("%$ingredients%")
            }
            if (instructions != null) {
                queryConditions += " r.instructions LIKE ? AND"
                params.add("%$instructions%")
            }

            // Remove the last " AND" operator
            queryConditions = queryConditions.dropLast(4)
        }

        return SimpleSQLiteQuery(
            "${queryBase}${queryJoins}${queryConditions}",
            params.toTypedArray()
        )
    }

    private fun isFiltered(): Boolean {
        return title != null ||
                category != null ||
                durationFrom != null ||
                durationTo != null ||
                portionsFrom != null ||
                portionsTo != null ||
                ingredients != null ||
                instructions != null
    }
}