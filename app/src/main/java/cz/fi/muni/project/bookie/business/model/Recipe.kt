package cz.fi.muni.project.bookie.business.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity
@Parcelize
data class Recipe(
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB) var image: ByteArray? = null,
    var title: String,
    var categoryId: Long? = null,
    var duration: Int? = null,
    var portions: Int? = null,
    var ingredients: String,
    var instructions: String
) : BaseEntity() {
    override fun hashCode(): Int {
        return (id?.hashCode() ?: 0) +
                (image?.hashCode() ?: 0) +
                title.hashCode() +
                (categoryId?.hashCode() ?: 0) +
                (duration?.hashCode() ?: 0) +
                (portions?.hashCode() ?: 0) +
                ingredients.hashCode() +
                instructions.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || other !is Recipe) return false

        return id == other.id &&
                compareImages(image, other.image) &&
                title == other.title &&
                categoryId == other.categoryId &&
                duration == other.duration &&
                portions == other.portions &&
                ingredients == other.ingredients &&
                instructions == other.instructions
    }

    override fun toString(): String {
        val shortIngredients = StringBuilder(ingredients)
        if (shortIngredients.length >= strMaxLength) {
            shortIngredients.setLength(strMaxLength)
            shortIngredients.append("...")
        }

        val shortInstructions = StringBuilder(instructions)
        if (shortInstructions.length >= strMaxLength) {
            shortInstructions.setLength(strMaxLength)
            shortInstructions.append("...")
        }

        return "Recipe{" +
                "id=$id" +
                ", title='$title'" +
                ", categoryId=$categoryId" +
                ", duration=$duration" +
                ", portions=$portions" +
                ", ingredients='$shortIngredients'" +
                ", instructions='$shortInstructions'" +
                "}"
    }

    private fun compareImages(image1: ByteArray?, image2: ByteArray?): Boolean {
        return if (image1 == null || image2 == null) listOf(image1, image2).all { it == null }
        else image1.contentEquals(image2)
    }
}