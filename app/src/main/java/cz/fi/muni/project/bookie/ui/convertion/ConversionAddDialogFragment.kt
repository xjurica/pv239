package cz.fi.muni.project.bookie.ui.convertion

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.google.android.material.textfield.TextInputLayout
import com.wajahatkarim3.easyvalidation.core.view_ktx.validator
import cz.fi.muni.project.bookie.ConversionsActivity
import cz.fi.muni.project.bookie.R
import cz.fi.muni.project.bookie.business.model.Conversion
import kotlinx.coroutines.runBlocking

class ConversionAddDialogFragment(
    private val fragment: ConversionListFragment
) : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val activity = requireActivity()
        val dialogView = View.inflate(activity, R.layout.dialog_add_conversion, null)
        val alertDialog = AlertDialog.Builder(activity, R.style.CustomDialogTheme).create()


        dialogView.findViewById<Button>(R.id.button_add_conversion).setOnClickListener {
            val from = dialogView.findViewById<TextInputLayout>(R.id.from_unit_textfield)
            val to = dialogView.findViewById<TextInputLayout>(R.id.to_unit_textfield)

            if (validateInput(from, to)) {
                addConversion(from, to)
                alertDialog.dismiss()
            }
        }

        dialogView.findViewById<Button>(R.id.button_add_conversion_cancel).setOnClickListener {
            alertDialog.dismiss()
        }

        alertDialog.setView(dialogView)
        return alertDialog
    }

    private fun addConversion(from: TextInputLayout, to: TextInputLayout) {
        val fromEditText = from.editText
        val toEditText = to.editText

        val conversion = Conversion(
            from = fromEditText?.text.toString(),
            to = toEditText?.text.toString()
        )

        runBlocking {
            (activity as ConversionsActivity).conversionDao.create(conversion)
        }

        Log.d("db", conversion.toString())

        Toast.makeText(context, "Conversion was successfully created", Toast.LENGTH_SHORT).show()
        fragment.updateAdapter()
    }

    private fun validateInput(from: TextInputLayout, to: TextInputLayout) : Boolean {
        var fromCheck = false
        var toCheck = false

        from.error = null
        to.error = null

        if (from.editText != null) {
            fromCheck = from.editText!!.validator()
                .nonEmpty("From Unit must be filled!")
                .addErrorCallback {
                    from.error = it
                }
                .check()
        }

        if (to.editText != null) {
            toCheck = to.editText!!.validator()
                .nonEmpty("To Unit must be filled!")
                .addErrorCallback {
                    to.error = it
                }
                .check()
        }

        return fromCheck && toCheck
    }
}