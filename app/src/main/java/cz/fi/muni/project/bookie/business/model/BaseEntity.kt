package cz.fi.muni.project.bookie.business.model

import android.os.Parcelable
import androidx.room.Ignore
import kotlinx.parcelize.IgnoredOnParcel

abstract class BaseEntity: Parcelable {
    @IgnoredOnParcel
    @Ignore
    val strMaxLength = 20
}