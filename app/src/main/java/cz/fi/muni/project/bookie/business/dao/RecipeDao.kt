package cz.fi.muni.project.bookie.business.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.RawQuery
import androidx.room.Update
import androidx.sqlite.db.SupportSQLiteQuery
import cz.fi.muni.project.bookie.business.model.Recipe

@Dao
interface RecipeDao {
    @Insert
    suspend fun create(vararg recipe: Recipe): List<Long>

    @Update
    suspend fun update(vararg recipe: Recipe)

    @Delete
    suspend fun delete(vararg recipe: Recipe)

    @Query("SELECT * FROM recipe")
    suspend fun findAll(): List<Recipe>

    @RawQuery
    suspend fun findFiltered(query: SupportSQLiteQuery): List<Recipe>

    @Query("SELECT * FROM recipe WHERE categoryId = :categoryId")
    suspend fun findByCategoryId(categoryId: Long): List<Recipe>
}