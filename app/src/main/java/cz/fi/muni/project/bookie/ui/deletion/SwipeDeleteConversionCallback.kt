package cz.fi.muni.project.bookie.ui.deletion

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import cz.fi.muni.project.bookie.business.dao.ConversionDao
import cz.fi.muni.project.bookie.ui.adapters.ConversionAdapter
import kotlinx.coroutines.runBlocking

class SwipeDeleteConversionCallback(private val adapter: ConversionAdapter,
                                    private val childFragmentManager: FragmentManager,
                                    context: Context?,
                                    private val dao: ConversionDao
) : SwipeDeleteCallback(context) {

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position = viewHolder.adapterPosition
        val conversions = adapter.getData() ?: return
        val conversion = conversions[position]
        val conversionToShow = "${conversion.from} = ${conversion.to}"

        val dialog = ConfirmDeletionDialogFragment(conversionToShow)
        dialog.listener = object : ConfirmDeletionDialogListener {
            override fun onConfirmDeletionResult(confirmed: Boolean) {
                if (confirmed) {
                    runBlocking {
                        dao.delete(conversion)
                    }
                    adapter.deleteItem(position)
                } else {
                    adapter.notifyItemChanged(position)
                }
            }
        }
        dialog.show(childFragmentManager, "ConfirmDeletionDialogFragment")
    }
}