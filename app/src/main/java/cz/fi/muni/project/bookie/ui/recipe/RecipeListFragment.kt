package cz.fi.muni.project.bookie.ui.recipe

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import cz.fi.muni.project.bookie.RecipesActivity
import cz.fi.muni.project.bookie.business.db.queryBuilder.RecipeQueryBuilder
import cz.fi.muni.project.bookie.databinding.FragmentRecipeListBinding
import cz.fi.muni.project.bookie.ui.adapters.RecipeAdapter
import cz.fi.muni.project.bookie.ui.deletion.SwipeDeleteRecipeCallback
import kotlinx.coroutines.runBlocking

class RecipeListFragment: Fragment() {
    private lateinit var binding: FragmentRecipeListBinding
    private val filterDialog: RecipeFilterDialogFragment = RecipeFilterDialogFragment(this::getFilteredRecipes)
    private lateinit var adapter:RecipeAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRecipeListBinding.inflate(inflater, container, false)

        val filterButton = binding.floatingActionButtonFilterRecipes
        filterButton.setOnClickListener {
            filterDialog.show(childFragmentManager, "Recipe filter")
        }

        val addButton = binding.floatingActionButtonAddRecipe
        addButton.setOnClickListener {
            findNavController().navigate(
                RecipeListFragmentDirections.actionNavigationRecipesToNavigationAddRecipe()
            )
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        adapter = RecipeAdapter { recipe ->
            val action = RecipeListFragmentDirections.actionNavigationRecipesToNavigationRecipeInfo(recipe)
            findNavController().navigate(action)
        }
        binding.recyclerView.adapter = adapter

        enableSwipeToDelete()
        getFilteredRecipes()
    }

    private fun showMessageWhenEmpty() {
        // When adapter is empty, show message instead of empty list.
        if (adapter.getData()?.isNotEmpty() == true) {
            binding.recyclerView.visibility = View.VISIBLE
            binding.textView.visibility = View.GONE
        }
        else {
            binding.recyclerView.visibility = View.GONE
            binding.textView.visibility = View.VISIBLE
        }
    }

    private fun getFilteredRecipes() {
        val queryBuilder = RecipeQueryBuilder()

        if (filterDialog.title != null) queryBuilder.filterByTitle(filterDialog.title!!)
        if (filterDialog.category != null) queryBuilder.filterByCategory(filterDialog.category!!)
        if (filterDialog.durationFrom != null && filterDialog.durationTo != null) {
            queryBuilder.filterByDuration(filterDialog.durationFrom!!, filterDialog.durationTo!!)
        }
        if (filterDialog.portionsFrom != null && filterDialog.portionsTo != null) {
            queryBuilder.filterByPortions(filterDialog.portionsFrom!!, filterDialog.portionsTo!!)
        }
        if (filterDialog.ingredients != null) queryBuilder.filterByIngredients(filterDialog.ingredients!!)
        if (filterDialog.instructions != null) queryBuilder.filterByInstructions(filterDialog.instructions!!)

        val recipes = runBlocking {
            (activity as RecipesActivity).recipeDao.findFiltered(queryBuilder.build())
        }

        adapter.submitList(recipes)
        showMessageWhenEmpty()
    }

    private fun enableSwipeToDelete() {
        val swipeToDeleteCallback = SwipeDeleteRecipeCallback(
            adapter,
            childFragmentManager,
            context,
            (activity as RecipesActivity).recipeDao,
            (activity as RecipesActivity).categoryDao,
            this::showMessageWhenEmpty
        )
        val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchHelper.attachToRecyclerView(binding.recyclerView)
    }
}
