package cz.fi.muni.project.bookie.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import cz.fi.muni.project.bookie.business.model.Conversion
import cz.fi.muni.project.bookie.databinding.ListItemConversionBinding

class ConversionAdapter(private val onClick: (Conversion) -> Unit) : ListAdapter<Conversion, ConversionViewHolder>(
    ConversionDiffUtil()
) {
    private var conversions: MutableList<Conversion>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConversionViewHolder =
        ConversionViewHolder(
            ListItemConversionBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ConversionViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, onClick)
    }

    override fun submitList(list: List<Conversion>?) {
        if (list != null) {
            conversions = list.toMutableList()
        }
        super.submitList(list)
    }

    fun getData(): MutableList<Conversion>? {
        return conversions
    }

    fun deleteItem(position: Int) {
        if (conversions != null) {
            conversions!!.removeAt(position)
            submitList(conversions)
        }
    }
}

class ConversionViewHolder(private val binding: ListItemConversionBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Conversion, onClick: (Conversion) -> Unit) {
        val string = "${item.from} = ${item.to}"
        binding.textViewConversion.text = string
        binding.root.setOnClickListener {
            onClick(item)
        }
    }
}

class ConversionDiffUtil : DiffUtil.ItemCallback<Conversion>() {

    override fun areItemsTheSame(oldItem: Conversion, newItem: Conversion): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Conversion, newItem: Conversion): Boolean =
        oldItem.equals(newItem)
}