package cz.fi.muni.project.bookie.business.db

import androidx.room.Database
import androidx.room.RoomDatabase
import cz.fi.muni.project.bookie.business.dao.CategoryDao
import cz.fi.muni.project.bookie.business.dao.ConversionDao
import cz.fi.muni.project.bookie.business.dao.RecipeDao
import cz.fi.muni.project.bookie.business.model.Category
import cz.fi.muni.project.bookie.business.model.Conversion
import cz.fi.muni.project.bookie.business.model.Recipe

@Database(entities = [Recipe::class, Category::class, Conversion::class], version = 1)
abstract class CookbookDatabase : RoomDatabase() {
    abstract fun recipeDao(): RecipeDao

    abstract fun categoryDao(): CategoryDao

    abstract fun conversionDao(): ConversionDao
}