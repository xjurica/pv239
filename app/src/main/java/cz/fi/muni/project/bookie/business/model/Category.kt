package cz.fi.muni.project.bookie.business.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(indices = [Index(value = ["name"], unique = true)])
@Parcelize
class Category(
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
    var name: String
) : BaseEntity() {
    override fun hashCode(): Int {
        return (id?.hashCode() ?: 0) +
                name.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || other !is Category) return false

        return id == other.id &&
                name == other.name
    }

    override fun toString(): String {
        return "Category{" +
                "id=$id" +
                ", name='$name'" +
                "}"
    }
}