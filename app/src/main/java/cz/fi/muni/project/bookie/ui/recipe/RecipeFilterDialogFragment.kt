package cz.fi.muni.project.bookie.ui.recipe

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.google.android.material.textfield.TextInputLayout
import com.wajahatkarim3.easyvalidation.core.view_ktx.validator
import cz.fi.muni.project.bookie.R
import cz.fi.muni.project.bookie.RecipesActivity

class RecipeFilterDialogFragment(private val filterRecipesCallback: () -> Unit): DialogFragment() {
    private lateinit var dialogView: View
    private lateinit var alertDialog: AlertDialog

    // Regex to parse/validate fields with a number or range of numbers.
    private val rangeRegex = Regex("^([0-9]+)(?:\\s*-\\s*([0-9]+))?\$")

    // Fields to be processed by query builder
    var title: String? = null
    var category: String? = null
    var durationFrom: Int? = null
    var durationTo: Int? = null
    var portionsFrom: Int? = null
    var portionsTo: Int? = null
    var ingredients: String? = null
    var instructions: String? = null

    // Fields to keep original input that we cannot reassemble with original formatting
    private var duration: String? = null
    private var portions: String? = null

    // TextFields
    private lateinit var titleTextField: TextInputLayout
    private lateinit var categoryTextField: TextInputLayout
    private lateinit var durationTextField: TextInputLayout
    private lateinit var portionsTextField: TextInputLayout
    private lateinit var ingredientsTextField: TextInputLayout
    private lateinit var instructionsTextField: TextInputLayout

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val activity = requireActivity()
        dialogView = View.inflate(activity, R.layout.dialog_filter_recipes, null)
        alertDialog = AlertDialog.Builder(activity, R.style.CustomDialogTheme).create()

        titleTextField = dialogView.findViewById(R.id.title_textfield)
        categoryTextField = dialogView.findViewById(R.id.category_textfield)
        durationTextField = dialogView.findViewById(R.id.duration_textfield)
        portionsTextField = dialogView.findViewById(R.id.portions_textfield)
        ingredientsTextField = dialogView.findViewById(R.id.ingredients_textfield)
        instructionsTextField = dialogView.findViewById(R.id.instructions_textfield)

        loadFilterFields()

        dialogView.findViewById<Button>(R.id.button_filter_set).setOnClickListener {
            if (validateInput()) {
                saveFilterFields()
                filterRecipesCallback()
                alertDialog.dismiss()
            }
        }

        dialogView.findViewById<Button>(R.id.button_filter_reset).setOnClickListener {
            resetFilter()
        }

        val categoryAutoCompleteTextView = dialogView.findViewById<AutoCompleteTextView>(R.id.autocomplete_category)
        ArrayAdapter(
            requireContext(),
            R.layout.autocomplete_item_category,
            R.id.text_view_autocomplete,
            (activity as RecipesActivity).categoryService.getAllNames()
        ).also {
            categoryAutoCompleteTextView.setAdapter(it)
        }

        alertDialog.setView(dialogView)
        return alertDialog
    }

    private fun loadFilterFields() {
        if (title != null && titleTextField.editText != null) titleTextField.editText!!.setText(title)
        if (category != null && categoryTextField.editText != null) categoryTextField.editText!!.setText(category)
        if (duration != null && durationTextField.editText != null) durationTextField.editText!!.setText(duration)
        if (portions != null && portionsTextField.editText != null) portionsTextField.editText!!.setText(portions)
        if (ingredients != null && ingredientsTextField.editText != null) ingredientsTextField.editText!!.setText(ingredients)
        if (instructions != null && instructionsTextField.editText != null) instructionsTextField.editText!!.setText(instructions)
    }

    private fun saveFilterFields() {

        if (titleTextField.editText != null) { title = convertEditText(titleTextField.editText!!) }
        if (categoryTextField.editText != null) { category = convertEditText(categoryTextField.editText!!) }
        if (durationTextField.editText != null) {
            with(convertEditTextNumbers(durationTextField.editText!!)) {
                durationFrom = first
                durationTo = second

                duration = if (durationFrom == null || durationTo == null) null else
                    durationTextField.editText!!.text.toString()
            }
        }
        if (portionsTextField.editText != null) {
            with(convertEditTextNumbers(portionsTextField.editText!!)) {
                portionsFrom = first
                portionsTo = second

                portions = if (portionsFrom == null || portionsTo == null) null else
                    portionsTextField.editText!!.text.toString()
            }
        }
        if (ingredientsTextField.editText != null) { ingredients = convertEditText(ingredientsTextField.editText!!) }
        if (instructionsTextField.editText != null) { instructions = convertEditText(instructionsTextField.editText!!) }
    }

    private fun resetFilter() {
        if (titleTextField.editText != null) titleTextField.editText!!.text.clear()
        if (categoryTextField.editText != null) categoryTextField.editText!!.text.clear()
        if (durationTextField.editText != null) durationTextField.editText!!.text.clear()
        if (portionsTextField.editText != null) portionsTextField.editText!!.text.clear()
        if (ingredientsTextField.editText != null) ingredientsTextField.editText!!.text.clear()
        if (instructionsTextField.editText != null) instructionsTextField.editText!!.text.clear()
    }

    private fun validateInput(): Boolean {
        var durationCheck = true
        if (durationTextField.editText != null) {
            val textInput = durationTextField.editText!!.text.toString()
            durationCheck = if (textInput != "") {
                durationTextField.editText!!.validator()
                    .regex(rangeRegex.toString(), "Should contain only number or number range")
                    .addErrorCallback {
                        durationTextField.error = it
                    }
                    .check()
            } else true
        }

        var portionsCheck = true
        if (portionsTextField.editText != null) {
            val textInput = portionsTextField.editText!!.text.toString()
                portionsCheck = if (textInput != "") {
                portionsTextField.editText!!.validator()
                    .regex(rangeRegex.toString(), "Should contain only number or number range")
                    .addErrorCallback {
                        portionsTextField.error = it
                    }
                    .check()
            } else true
        }

        return durationCheck && portionsCheck
    }

    private fun convertEditText(editText: EditText): String? {
        val string = editText.text.toString()

        return if (string != "") string else null
    }

    private fun convertEditTextNumbers(editText: EditText): Pair<Int?, Int?> {
        val string = editText.text.toString()

        return parseNumbers(string)
    }

    private fun parseNumbers(string: String): Pair<Int?, Int?> {
        // Get numbers from string in format like "1", "1-2" or "1 - 2". When only one number is
        // given, it is set to both places in Pair.
        var num1: Int? = null; var num2: Int? = null

        val matchResult = rangeRegex.matchEntire(string)
        if (matchResult != null) {
            num1 = matchResult.groupValues[1].toIntOrNull()
            num2 = if (matchResult.groupValues[2] != "") matchResult.groupValues[2].toIntOrNull() else num1
        }

        return Pair(num1, num2)
    }
}