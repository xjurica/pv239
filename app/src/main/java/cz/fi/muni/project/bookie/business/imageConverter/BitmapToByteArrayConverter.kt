package cz.fi.muni.project.bookie.business.imageConverter

import android.graphics.Bitmap
import java.io.ByteArrayOutputStream

class BitmapToByteArrayConverter(
    private val image: Bitmap
) {
    fun convert(): ByteArray {
        val stream = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 90, stream)

        return stream.toByteArray()
    }
}