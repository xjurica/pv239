package cz.fi.muni.project.bookie.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import cz.fi.muni.project.bookie.business.imageConverter.ByteArrayToBitmapConverter
import cz.fi.muni.project.bookie.business.model.Recipe
import cz.fi.muni.project.bookie.databinding.ListItemRecipeBinding

class RecipeAdapter(private val onClick: (Recipe) -> Unit) : ListAdapter<Recipe, RecipeViewHolder>(
    RecipeDiffUtil()
) {
    private var recipes: MutableList<Recipe>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder =
        RecipeViewHolder(
            ListItemRecipeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, onClick)
    }

    override fun submitList(list: List<Recipe>?) {
        if (list != null) {
            recipes = list.toMutableList()
        }
        super.submitList(list)
    }

    fun getData(): MutableList<Recipe>? {
        return recipes
    }

    fun deleteItem(position: Int) {
        if (recipes != null) {
            recipes!!.removeAt(position)
            submitList(recipes)
        }
    }
}

class RecipeViewHolder(private val binding: ListItemRecipeBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(item: Recipe, onClick: (Recipe) -> Unit) {
        if (item.image != null) {
            binding.imageViewRecipe.setImageBitmap(
                ByteArrayToBitmapConverter(item.image!!).convert()
            )
        }
        binding.recipeTitle.text = item.title

        binding.root.setOnClickListener {
            onClick(item)
        }
    }
}

class RecipeDiffUtil : DiffUtil.ItemCallback<Recipe>() {

    override fun areItemsTheSame(oldItem: Recipe, newItem: Recipe): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Recipe, newItem: Recipe): Boolean =
        oldItem.equals(newItem)
}