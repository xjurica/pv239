package cz.fi.muni.project.bookie.business.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import cz.fi.muni.project.bookie.business.model.Conversion

@Dao
interface ConversionDao {
    @Insert
    suspend fun create(vararg conversion: Conversion): List<Long>

    @Update
    suspend fun update(vararg conversion: Conversion)

    @Delete
    suspend fun delete(vararg conversion: Conversion)

    @Query("SELECT * FROM conversion")
    suspend fun findAll(): List<Conversion>
}