package cz.fi.muni.project.bookie.ui.recipe

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.wajahatkarim3.easyvalidation.core.view_ktx.validator
import cz.fi.muni.project.bookie.R
import cz.fi.muni.project.bookie.RecipesActivity
import cz.fi.muni.project.bookie.business.fileLoader.ImageLoader
import cz.fi.muni.project.bookie.business.imageConverter.BitmapToByteArrayConverter
import cz.fi.muni.project.bookie.business.model.Recipe
import cz.fi.muni.project.bookie.databinding.FragmentAddRecipeBinding
import kotlinx.coroutines.runBlocking

class RecipeAddFragment : Fragment() {
    private lateinit var binding: FragmentAddRecipeBinding
    private lateinit var imageLoader: ImageLoader
    private lateinit var defaultImage: Bitmap

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddRecipeBinding.inflate(inflater, container, false)
        imageLoader = ImageLoader(this, requireActivity())

        val imageButton = binding.buttonAddRecipeImage
        val imageView = binding.imageViewRecipe
        imageButton.setOnClickListener {
            imageLoader.pick(imageView)
        }
        val bd: BitmapDrawable = imageView.drawable as BitmapDrawable
        defaultImage = bd.bitmap

        val categoryAutoCompleteTextView = binding.categoryTextfield.editText as AutoCompleteTextView
        ArrayAdapter(
            requireContext(),
            R.layout.autocomplete_item_category,
            R.id.text_view_autocomplete,
            (activity as RecipesActivity).categoryService.getAllNames()
        ).also {
            categoryAutoCompleteTextView.setAdapter(it)
        }

        val cancelButton = binding.buttonAddRecipeCancel
        cancelButton.setOnClickListener {
            findNavController().navigate(
                RecipeAddFragmentDirections.actionNavigationAddRecipeToNavigationRecipes()
            )
        }

        val addButton = binding.buttonAddRecipe
        addButton.setOnClickListener {
            if (validateInput()) {
                addRecipe()

                findNavController().navigate(
                    RecipeAddFragmentDirections.actionNavigationAddRecipeToNavigationRecipes()
                )
            }
        }

        return binding.root
    }

    private fun addRecipe() {
        var blob: ByteArray? = null
        val imageView = binding.imageViewRecipe
        val bd: BitmapDrawable = imageView.drawable as BitmapDrawable
        val bitmap = bd.bitmap
        if (defaultImage != bitmap) blob = BitmapToByteArrayConverter(bitmap).convert()

        val title: String = binding.titleTextfield.editText?.text.toString()

        val categoryString = (binding.categoryTextfield.editText as AutoCompleteTextView).text.toString().ifBlank {"Unspecified"}
        val categoryId = (activity as RecipesActivity)
            .categoryService
            .getId(
                categoryString,
                createWhenMissing = true
            )

        val recipe = Recipe(
            image = blob,
            title = title,
            categoryId = categoryId,
            duration = binding.durationTextfield.editText?.text.toString().toIntOrNull(),
            portions = binding.portionsTextfield.editText?.text.toString().toIntOrNull(),
            ingredients = binding.ingredientsTextfield.editText?.text.toString(),
            instructions = binding.instructionsTextfield.editText?.text.toString()
        )

        runBlocking {
            (activity as RecipesActivity).recipeDao.create(recipe)
        }

        Toast.makeText(
            context,
            "Successfully created recipe ${title}",
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun validateInput() : Boolean {
        var titleCheck = false
        var durationCheck = false
        var portionsCheck = false

        val title = binding.titleTextfield
        title.error = null
        if (title.editText != null) {
            titleCheck = title.editText!!.validator()
                .nonEmpty("Title must be filled!")
                .addErrorCallback {
                    title.error = it
                }
                .check()
        }

        val duration = binding.durationTextfield
        duration.error = null
        if (duration.editText != null) {
            durationCheck = duration.editText!!.validator()
                .nonEmpty("Duration must be filled!")
                .validNumber()
                .greaterThanOrEqual(0)
                .addErrorCallback {
                    duration.error = it
                }
                .check()
            if (durationCheck && duration.editText!!.text.toString().toIntOrNull() == null) {
                durationCheck = false
                duration.error = "Number must be below 2147483648"
            }
        }

        val portions = binding.portionsTextfield
        portions.error = null
        if (portions.editText != null) {
            portionsCheck = portions.editText!!.validator()
                .nonEmpty("Portions must be filled!")
                .validNumber()
                .greaterThanOrEqual(0)
                .addErrorCallback {
                    portions.error = it
                }
                .check()
            if (portionsCheck && portions.editText!!.text.toString().toIntOrNull() == null) {
                portionsCheck = false
                portions.error = "Number must be below 2147483648"
            }
        }

        return titleCheck &&
                durationCheck &&
                portionsCheck
    }
}