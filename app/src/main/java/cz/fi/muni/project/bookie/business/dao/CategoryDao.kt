package cz.fi.muni.project.bookie.business.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import cz.fi.muni.project.bookie.business.model.Category

@Dao
interface CategoryDao {
    @Insert
    suspend fun create(vararg category: Category): List<Long>

    @Update
    suspend fun update(vararg category: Category)

    @Delete
    suspend fun delete(vararg category: Category)

    @Query("DELETE FROM category WHERE id = :id")
    suspend fun deleteById(id: Long)

    @Query("SELECT * FROM category")
    suspend fun findAll(): List<Category>

    @Query("SELECT * FROM category WHERE name = :name")
    suspend fun findByName(name: String): List<Category>

    @Query("SELECT * FROM category WHERE id = :id")
    suspend fun findById(id: Long): List<Category>
}