# CookBook

CookBook is an Android application designed to serve as a comprehensive recipe database.

Developed in Kotlin, this app allows users to perform CRUD (Create, Read, Update, Delete) operations on recipes, filter recipes based on various criteria, and add images to recipes with automatic aspect ratio adjustments.

Additionally, CookBook includes a comparison Activity for unit comparison, making it easier to note down conversions.

## Features

- **CRUD Operations**: Easily create, read, update, and delete recipes.
- **Recipe Filtering**: Filter recipes based on ingredients, cooking time, difficulty, and other parameters.
- **Image Handling**: Add images to recipes. Non square images are filled with blurred sides to maintain aesthetic consistency.
- **Unit Comparison**: Add unit conversions as notes.

## Installation

To set up the CookBook project on your local machine, follow these steps:

1. **Clone the repository:**
   ```sh
   git clone https://github.com/yourusername/CookBook.git
   ```

2. **Open the project in Android Studio:**
    - Open Android Studio.
    - Select `File > Open`.
    - Navigate to the cloned repository's directory and select it.

3. **Build the project:**
    - Click on the `Build` menu and select `Make Project` or press `Ctrl+F9`.

4. **Run the project:**
    - Click the `Run` button or press `Shift+F10`.

## Usage

### Adding a Recipe

1. Click the `Add Recipe` button on the main screen.
2. Fill in the recipe details (name, ingredients, instructions, etc.).
3. Add an image for the recipe. If the image is not square, the app will automatically adjust it with blurred sides.
4. Save the recipe.

### Managing Recipes

- **Update a Recipe**: Select a recipe from the list and click the `Edit` button to update its details.
- **Delete a Recipe**: Swipe a recipe from the list to the left and click the `Delete` button to remove it.

### Filtering Recipes

1. Use the filter icon on the main screen.
2. Set your desired filter criteria (e.g., ingredient type, cooking time).
3. Apply the filter to view the filtered list of recipes.

### Unit Comparison

1. Navigate to the comparison Activity from the main screen.
2. Add a new conversion by entering the units and their amounts.
3. Save the conversion for future reference.

## Authors

- 445626 - Bc. Jiří Juřica
- 536592 - Michal Pataky

---

Happy cooking!
